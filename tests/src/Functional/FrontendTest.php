<?php

declare(strict_types = 1);

namespace Drupal\Tests\licenses\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group licenses
 */
final class FrontendTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'views', 'licenses'];

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Test callback.
   */
  public function testSomething(): void {
    $admin_user = $this->drupalCreateUser(['access site licenses']);
    $this->drupalLogin($admin_user);

    $this->drupalGet('admin/reports/licenses');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Licenses"]');

    $this->assertSession()->elementExists('xpath', '//fieldset/legend/span[text() = "Licenses overview"]');

    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td/a[text() = "GNU General Public License v2.0 or later"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td[text() = "GPL-2.0-or-later"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td[text() = "Yes, is OSI certified"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td/a[text() = "MIT License"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td[text() = "MIT"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr/td[text() = "Yes, is OSI certified"]');

    $this->assertSession()->elementExists('xpath', '//fieldset/legend/span[text() = "Libraries and licenses"]');

    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/summary[text() = "GNU General Public License v2.0 or later"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/summary[text() = "MIT License"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "GPL-2.0-or-later"]');

    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "core/drupal"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "core/drupalSettings"]');

    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "claro.libraries.yml"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "core.libraries.yml"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "block.libraries.yml"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "system.libraries.yml"]');
    $this->assertSession()->elementExists('xpath', '//fieldset/div/details/div/table/tbody/tr/td[text() = "views.libraries.yml"]');
  }

}
