/**
 * @file
 * Licenses js file.
 */

(($, Drupal, drupalSettings, once) => {
  Drupal.behaviors.licenses = {
    attach(context) {
      const licensesText = once(
        'licenses-text',
        '.licenses--license-text',
        context,
      );
      licensesText.forEach((text) => {
        $(text).hide();
      });

      const licensesLinksFull = once(
        'licenses-link-full',
        '.licenses--full-license-link a',
        context,
      );
      licensesLinksFull.forEach((link) => {
        $(link).click((e) => {
          e.preventDefault();
          $(link).closest('tr').next().toggle();
        });
      });

      const licensesTogglesAll = once(
        'licenses-toggle-all',
        '.licenses--license-toggle-all',
        context,
      );
      licensesTogglesAll.forEach((toggle) => {
        $(toggle).click((e) => {
          e.preventDefault();
          $('details summary').click().blur();
          $('.licenses--full-license-link a', context).click();
        });
      });
    },
  };
})(jQuery, Drupal, drupalSettings, once);
