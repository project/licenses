<?php

namespace Drupal\licenses\Plugin\License\Scanner;

use Composer\Spdx\SpdxLicenses;
use Drupal\Core\Asset\LibraryDiscovery;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\licenses\License;
use Drupal\licenses\LicensePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the license.
 *
 * @License(
 *   id = "module_libraries",
 *   label = @Translation("Module libraries"),
 *   description = @Translation("Drupal module defined libraries licenses.")
 * )
 */
final class ModuleLibraries extends LicensePluginBase implements ScannerInterface {

  /**
   * An array of licenses.
   *
   * @var \Drupal\licenses\License[]
   */
  protected $licenses = [];

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * {@inheritdoc}
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      ModuleHandler $module_handler,
      LibraryDiscovery $library_discovery,
      ModuleExtensionList $extension_list_module
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->libraryDiscovery = $library_discovery;
    $this->extensionListModule = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
    ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('library.discovery'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function scan() {
    $spdx = new SpdxLicenses();
    $modules = $this->moduleHandler->getModuleList();

    // Fake core as a module.
    $modules = ['core' => []] + $modules;

    foreach ($modules as $machine_name => $module) {
      $libraries = $this->libraryDiscovery
        ->getLibrariesByExtension($machine_name);

      foreach ($libraries as $library_machine_name => $library) {
        $license = new License();
        $identifier = str_replace('GNU-', '', (string) @$library['license']['name']);
        $spdx_license = $spdx->getLicenseByIdentifier($identifier);

        $license
          ->setIdentifier($identifier)
          ->setLibraryName($machine_name . '/' . $library_machine_name)
          ->setLibraryVersion((string) @$library['version'])
          ->setLibraryHomepage((string) @$library['remote'])
          ->setFilename(implode(DIRECTORY_SEPARATOR, [
            DRUPAL_ROOT,
            'core',
            'core.libraries.yml',
          ]));
        if ($machine_name !== 'core') {
          $license->setFilename(implode(DIRECTORY_SEPARATOR, [
            DRUPAL_ROOT,
            $this->extensionListModule->getPath($machine_name),
            $machine_name . '.libraries.yml',
          ]));
        }
        if (isset($spdx_license)) {
          $license
            ->setFullName((string) $spdx_license[0])
            ->setOsiCertified((bool) $spdx_license[1])
            ->setUri((string) $spdx_license[2])
            ->setDeprecated((bool) $spdx_license[3]);
        }
        array_push($this->licenses, $license);
      }
    }
    return $this->licenses;
  }

}
