<?php

namespace Drupal\licenses\Plugin\License\Scanner;

use Composer\Spdx\SpdxLicenses;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\licenses\License;
use Drupal\licenses\LicensePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the license.
 *
 * @License(
 *   id = "nodejs",
 *   label = @Translation("Node.js"),
 *   description = @Translation("Node.js defined libraries licenses.")
 * )
 */
final class Nodejs extends LicensePluginBase implements ScannerInterface {

  /**
   * An array of licenses.
   *
   * @var \Drupal\licenses\License[]
   */
  protected $licenses = [];

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  protected $themeHandler;

  /**
   * The theme extension list service.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $extensionListTheme;

  /**
   * {@inheritdoc}
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      ThemeHandler $theme_handler,
      ThemeExtensionList $extension_list_theme
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->themeHandler = $theme_handler;
    $this->extensionListTheme = $extension_list_theme;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
    ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('theme_handler'),
      $container->get('extension.list.theme')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function scan() {
    $spdx = new SpdxLicenses();

    $themes = $this->themeHandler->listInfo();
    foreach ($themes as $machine_name => $theme) {
      $filename = implode(DIRECTORY_SEPARATOR, [
        DRUPAL_ROOT,
        $this->extensionListTheme->getPath($machine_name),
        'package-lock.json',
      ]);
      if (!file_exists($filename)) {
        continue;
      }

      $libraries = json_decode(file_get_contents($filename));
      foreach ($libraries->dependencies as $library_machine_name => $library) {
        $license = new License();
        $identifier = 'n/a';
        $spdx_license = $spdx->getLicenseByIdentifier($identifier);

        $license
          ->setIdentifier($identifier)
          ->setLibraryName($library_machine_name)
          ->setLibraryVersion((string) $library->version)
          ->setFilename($filename);
        if (isset($spdx_license)) {
          $license
            ->setFullName((string) $spdx_license[0])
            ->setOsiCertified((bool) $spdx_license[1])
            ->setUri((string) $spdx_license[2])
            ->setDeprecated((bool) $spdx_license[3]);
        }
        array_push($this->licenses, $license);
      }
    }
    return $this->licenses;
  }

}
