<?php

namespace Drupal\licenses\Plugin\License\Scanner;

use Composer\Spdx\SpdxLicenses;
use Drupal\Core\Asset\LibraryDiscovery;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\licenses\License;
use Drupal\licenses\LicensePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the license.
 *
 * @License(
 *   id = "theme_libraries",
 *   label = @Translation("Theme libraries"),
 *   description = @Translation("Drupal theme defined libraries licenses.")
 * )
 */
final class ThemeLibraries extends LicensePluginBase implements ScannerInterface {

  /**
   * An array of licenses.
   *
   * @var \Drupal\licenses\License[]
   */
  protected $licenses = [];

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  protected $themeHandler;

  /**
   * The library discovery service..
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * The theme ectension list service.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $extensionListTheme;

  /**
   * {@inheritdoc}
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      ThemeHandler $theme_handler,
      LibraryDiscovery $library_discovery,
      ThemeExtensionList $extension_list_theme
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->themeHandler = $theme_handler;
    $this->libraryDiscovery = $library_discovery;
    $this->extensionListTheme = $extension_list_theme;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
    ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('theme_handler'),
      $container->get('library.discovery'),
      $container->get('extension.list.theme')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function scan() {
    $spdx = new SpdxLicenses();

    $themes = $this->themeHandler->listInfo();
    foreach ($themes as $machine_name => $module) {
      $libraries = $this->libraryDiscovery
        ->getLibrariesByExtension($machine_name);

      foreach ($libraries as $library_machine_name => $library) {
        $license = new License();
        $identifier = str_replace('GNU-', '', (string) @$library['license']['name']);
        $spdx_license = $spdx->getLicenseByIdentifier($identifier);

        $license
          ->setIdentifier($identifier)
          ->setLibraryName($machine_name . '/' . $library_machine_name)
          ->setLibraryVersion((string) @$library['version'])
          ->setLibraryHomepage((string) @$library['remote'])
          ->setFilename(implode(DIRECTORY_SEPARATOR, [
            DRUPAL_ROOT,
            $this->extensionListTheme->getPath($machine_name),
            $machine_name . '.libraries.yml',
          ]));
        if (isset($spdx_license)) {
          $license
            ->setFullName((string) $spdx_license[0])
            ->setOsiCertified((bool) $spdx_license[1])
            ->setUri((string) $spdx_license[2])
            ->setDeprecated((bool) $spdx_license[3]);
        }
        array_push($this->licenses, $license);
      }
    }
    return $this->licenses;
  }

}
